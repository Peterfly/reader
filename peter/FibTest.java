import org.junit.*;
import static org.junit.Assert.*;
import helloworld.*;

public class FibTest {
	public int xx = 0;

	@Test
	public void testPass() {
		LogHelper.setupEnvironment("<Common.DynamicAssertion object at 0x10fcb0490>", new String[] { "Fib#calculateFib(int)" });
assertEquals(Fib.calculateFib(10), 55);
LogHelper.destroyEnvironment("<Common.DynamicAssertion object at 0x10fcb0490>");

	}

	@Test
	public void testFail() {
		LogHelper.setupEnvironment("<Common.DynamicAssertion object at 0x10fcb01d0>", new String[] {  });
assertEquals(Fib.calculateFib(8), 55);
LogHelper.destroyEnvironment("<Common.DynamicAssertion object at 0x10fcb01d0>");

	}

	public static void main(String ignored[]) {
		org.junit.runner.JUnitCore.main("FibTest");
	}
}