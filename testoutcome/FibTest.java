import org.junit.*;
import static org.junit.Assert.*;
import helloworld.*;

public class FibTest {
	public int xx = 0;

	@Test
	public void testPass() {
		LogHelper.setupEnvironment("Fib.calculateFib(10)", new String[] { "Fib#calculateFib(int)" });
assertEquals(Fib.calculateFib(10), 55);
LogHelper.destroyEnvironment("Fib.calculateFib(10)");

	}

	@Test
	public void testFail() {
		LogHelper.setupEnvironment("Fib.calculateFib(8)", new String[] {  });
assertEquals(Fib.calculateFib(8), 55);
LogHelper.destroyEnvironment("Fib.calculateFib(8)");

	}

	public static void main(String ignored[]) {
		org.junit.runner.JUnitCore.main("FibTest");
	}
}