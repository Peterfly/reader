[Original Proposal (not Reader++)](https://docs.google.com/document/d/1YzPIjw9h5pveL3n7IRldTjObPVrrN9PqzJkuqdRkS9Y/edit?usp=sharing)

[Design](https://docs.google.com/document/d/1dgBbwaxBF2jW2feNuR0R9_YjPH1f1rXFHSQd2DEHiiQ/edit?usp=sharing)

[Class Presentation Slides](https://docs.google.com/presentation/d/1xO92ovhyK2NG6Phr1NvHTNH1mvJqfJts6Dh-IlkVYXM/edit?usp=sharing)

[Poster](https://dl.dropboxusercontent.com/u/9735663/164-poster-1.ppt)

[Demo Video](https://dl.dropboxusercontent.com/u/6233550/Reader_demo.mov)
