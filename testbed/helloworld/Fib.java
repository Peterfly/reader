package helloworld;
public class Fib {
	public static int calculateFib(int a) {String args = "";args += String.valueOf(a);LogHelper.invocationBegin("Fib#calculateFib(int)", args);int ret = calculateFibPrime(a);LogHelper.invocationEnd("Fib#calculateFib(int)", String.valueOf(ret));return ret;} 	public static int calculateFibPrime(int nth) {
		// sanitize
		if (nth < 0) {
			return -1;
		}
		if (nth == 0) {
			return 0;
		} else if (nth == 1) {
			return 1;
		}
		return calculateFib(nth - 1) + calculateFib(nth - 2);
	}

	public static int foo() {
		
	}
	public static void main(String argvs[]) {
        LogHelper.setupEnvironment("test1", new String[] {"Fib#calculateFib(int)"});
		System.out.println("Fib(2) = " + calculateFib(2));
        LogHelper.destroyEnvironment("test1");
		System.out.println("(should not have log) Fib(10) = " + calculateFib(10));
        LogHelper.setupEnvironment("test2", new String[] {"Fib#calculateFib(int)"});
		System.out.println("Fib(20) = " + calculateFib(20));
        LogHelper.destroyEnvironment("test2");
	}
}
